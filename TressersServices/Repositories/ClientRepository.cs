﻿using System.Linq;
using TressersServices.Models;
using TressersServices.DataModel;

namespace TressersServices.Repositories
{
    public class ClientRepository
    {
        public ClientModel GetClient(int identifier)
        {
            using (var db = new Model1Container())
            {
                var client = ( from b in db.Clients
                             where b.Id == identifier
                             select new ClientModel()
                             {
                                 Name = b.Name
                             }).FirstOrDefault();
                return client;
                
            }
        }
    }
}