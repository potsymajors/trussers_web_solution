﻿using Microsoft.AspNet.Identity;
using System.Web.Http;
using TressersServices.Models;
using TressersServices.Repositories;

namespace TressersServices.Controllers
{
    public class ClientController : ApiController
    {
        private ClientRepository repo = new ClientRepository();

        // GET api/ClientInfo/1
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("ClientInfo")]
        public ClientModel GetClient(int identifier)
        {
            return repo.GetClient(identifier);
        }
    }
}